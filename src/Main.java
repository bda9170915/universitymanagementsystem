import model.Admin;
import model.Student;
import model.Teacher;
import model.User;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<User> users = new ArrayList<>();
        users.add(new Admin("xenakil","1234"));
        users.add(new Teacher("Ali","Ashrafli"));
        users.add(new Student("Elmin","1234"));
        Scanner input = new Scanner(System.in);
        while (true) {
            System.out.println("-----------------------------------------");
            System.out.println("1.Login \n2.Register\n3.Exit");
            System.out.println("-----------------------------------------");
            System.out.print("Choice: ");
            int choice = input.nextInt();
            switch (choice) {
                case 1:
                    login(users, input);
                    break;
                case 2:
                    register(users, input);
                    break;
                case 3:
                    System.out.println("Goodbye!");
                    System.exit(0);
                    break;
                default:
                    System.out.println("No such choice.....");
                    break;
            }
        }
    }
    public static void login(ArrayList<User> users, Scanner scanner) {
        System.out.println("-----------------------------------------");
        System.out.print("Enter the username: ");
        String username = scanner.next();
        System.out.print("Enter the password: ");
        String password = scanner.next();

        User loggedInUser = findUser(users, username, password);

        if (loggedInUser != null) {
            System.out.println("Login Successfully!");
            String userType = loggedInUser.getUserType();

            switch (userType) {
                case "admin":
                    System.out.println("-----------------------------------------");
                    System.out.println("Hello the God with the name of: " + loggedInUser.getUsername());
                    break;
                case "student":
                    System.out.println("-----------------------------------------");
                    System.out.println("Hello poor Student with the name of: " + loggedInUser.getUsername());
                    break;
                case "teacher":
                    System.out.println("-----------------------------------------");
                    System.out.println("Hello our respectable sensei: " + loggedInUser.getUsername());
                    break;
                default:
                    System.out.println("Unknown user type....");
            }
        } else {
            System.out.println("Login failed. Invalid username or password!");
        }
    }
    public static void register(ArrayList<User> users, Scanner scanner) {
        System.out.println("-----------------------------------------");
        System.out.print("Enter the username for registration: ");
        String username = scanner.next();
        for (User user : users) {
            if (user.getUsername().equals(username)) {
                System.out.println("Username is already taken....");
                return;
            }
        }
        System.out.print("Enter the password for registration: ");
        String password = scanner.next();

        System.out.print("Enter user type (admin/teacher/student): ");
        String userType = scanner.next().toLowerCase();

        switch (userType) {
            case "admin":
                users.add(new Admin(username, password));
                break;
            case "teacher":
                users.add(new Teacher(username, password));
                break;
            case "student" :
                users.add(new Student(username, password));
                break;
            default:
                System.out.println("No such choice...");
                break;
        }

        System.out.println("Registration successfully completed!");
    }
    private static User findUser(ArrayList<User> users, String username, String password) {
        for (User user : users) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }
}
