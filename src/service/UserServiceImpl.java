package service;

import model.User;

import java.util.ArrayList;
import java.util.Scanner;

public interface UserServiceImpl {
    void login(Scanner scanner,ArrayList<User> users);
    void register(Scanner scanner,ArrayList<User> users);
}
